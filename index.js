/*
	Requires
*/
var express = require('express');

var app = express();

/*
	Routes settings
*/
var api = require('./routes/api');
app.use('/api/', api);

/*
	Listening
*/
app.listen(8000,function(){
	console.log('Listening in localhost:8000')
})