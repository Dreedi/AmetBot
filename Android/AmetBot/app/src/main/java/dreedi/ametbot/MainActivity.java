package dreedi.ametbot;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.provider.MediaStore;
import android.widget.ImageView;
import java.util.logging.Logger;

import dreedi.ametbot.speaker.Speaker;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    EditText userMessage;
    Button userSendMessage;
    Button buttonLoadImage;
    private Speaker speaker;
    private static int RESULT_LOAD_IMAGE = 1;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        userMessage = (EditText)findViewById(R.id.userMensaje);
        userSendMessage = (Button)findViewById(R.id.userSendMessage);
        buttonLoadImage = (Button) findViewById(R.id.buttonLoadPicture);
        userSendMessage.setOnClickListener(this);
        buttonLoadImage.setOnClickListener(this);
        speaker = new Speaker(this);

    }

    @Override
    public void onClick(View v) {
        int idClicked = v.getId();
        if(idClicked==R.id.userSendMessage){
            String message = userMessage.getText().toString();
            speaker.speak(message);
        } else {
            if(idClicked==R.id.buttonLoadPicture){
                Log.d("OnClick","buttonLoadPicture");
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImageView imageView = (ImageView) findViewById(R.id.imgView);
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));

        }
    }

    @Override
    protected void onPause(){
        speaker.destroy();
        super.onPause();
    }

    @Override
    protected void onStop(){
        speaker.destroy();
        super.onPause();
    }

    @Override
    protected void onResume(){
        speaker = new Speaker(this);
        super.onResume();
    }

    @Override
    protected void onStart(){
        speaker = new Speaker(this);
        super.onStart();
    }

}
