/*
	Requires
*/
var express = require('express');
var bodyParser = require('body-parser');

/*
	Cleverbot settings
*/
var Cleverbot = require('../lib/cleverbot');
var CBots = new Cleverbot;
var name = 'Ametbot'
	
var app = express();

app.use(bodyParser.json('application/json'));

/*
	Routes
*/
app.post('/message/', function(req,res){
	console.log('user :' + req.body.message);
	if (typeof(req.body.message) != 'undefined') {
		var message = req.body.message;
		CBots.write(message, function callback(answer){
			console.log('AmetBot : ' + answer.message);
			return res
				.status(201)
				.json(answer);
		});
	} else {
		return res
			.status(400)
			.send('your request is wrong');
	}
});

/*
	Export
*/
module.exports=app;
